//
//  PdxBoundary.m
//  ACHD Reporter
//
//  Created by Mike Quetel on 2/8/10.
//  Copyright 2010 City of Portland. All rights reserved.
//
//  Portions Copyright 1994-2006 W Randolph Franklin (WRF)
//  http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
//

#import "PDXBoundary.h"
#import <CoreLocation/CoreLocation.h>

@implementation PDXBoundary


-(id)init
{
	self = [super init];
    if (self != nil) {
		// coordinate pairs that define a simplified Ada County boundary, used to hit-test the report location prior to allowing it to be uploaded
        
        NSUInteger setIndex = 0; // not sure why this array is hard coded, nor why it's not a native cocoa container. array size needs updating in header and below
        // Top left
        // 43.8268,-116.563568
		xCoords[0]   = -116.563568;
		yCoords[0] =   43.8268;
        
        // Top right
        // 43.852553,-116.159821
        xCoords[1]   = -116.159821;
		yCoords[1] =   43.852553;
        
        // Right Middle (lucky peak)
        // 43.596505,-115.893402
        xCoords[2]   = -115.893402;
		yCoords[2] =   43.596505;
        
        // Bottom Right (near mountain home)
        // 43.069088,-115.937347
        xCoords[3]   = -115.937347;
		yCoords[3] =   43.069088;
        
        // Bottom left
        // 43.08915,-116.332855
        xCoords[4]   = -116.332855;
		yCoords[4] =   43.08915;
        
        // Lower bottom left
        // 43.253405,-116.560822
        xCoords[5]   = -116.560822;
		yCoords[5] =   43.253405;
		
	}
	return self;
}

- (bool)locationIsInside:(CLLocation *)location
{
	// code adapted from http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
	bool returnValue = NO;
	if (location != nil)
    {
		int i,j,vertexCount = 6;
		float textX = location.coordinate.longitude;
		float testY = location.coordinate.latitude;
		
		for (i = 0, j = vertexCount-1; i < vertexCount; j = i++)
        {
			if ( ((yCoords[i]>testY) != (yCoords[j]>testY)) &&
				(textX < (xCoords[j]-xCoords[i]) * (testY-yCoords[i]) / (yCoords[j]-yCoords[i]) + xCoords[i]) )
            {
				returnValue = !returnValue;
            }
		}
	}
	return returnValue;
}



@end
