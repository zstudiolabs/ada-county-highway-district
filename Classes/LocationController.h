//
//  LocationController.h
//  ACHD Reporter
//
//  Created by Mike Quetel on 12/17/09.
//  Copyright 2009 City of Portland. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface  LocationController : NSObject <CLLocationManagerDelegate> {
    CLLocation *currentLocation;
	bool locationServicesAvailable;
}

+ (LocationController *)sharedInstance;

-(void) start;
-(void) stop;
-(BOOL) locationKnown;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *currentLocation;
@property (nonatomic) bool locationServicesAvailable;

@end
