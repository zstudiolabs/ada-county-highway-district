//
//  AddressLocationViewController.h
//  ACHD Reporter
//
//  Created by Michael Ziray on 11/1/12.
//
//

#import <UIKit/UIKit.h>

@interface AddressLocationViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITextField *streetAddress;

@property (retain, nonatomic) IBOutlet UITextField *intersection1;
@property (retain, nonatomic) IBOutlet UITextField *intersection2;
@property (retain, nonatomic) IBOutlet UITextView *additionalDirections;
@property (retain, nonatomic) IBOutlet UIButton *saveButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;

-(IBAction)setAddressTapped:(id)sender;

@end
