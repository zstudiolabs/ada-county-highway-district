//
//  AddressLocationViewController.m
//  ACHD Reporter
//
//  Created by Michael Ziray on 11/1/12.
//
//

#import "AddressLocationViewController.h"
#import "DataRepository.h"
#import "Report.h"

@interface AddressLocationViewController ()

@end

@implementation AddressLocationViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_saveButton setBackgroundImage:[[UIImage imageNamed:@"greenButton"] stretchableImageWithLeftCapWidth: 14 topCapHeight: 22]  forState: UIControlStateNormal];
    [_cancelButton setBackgroundImage:[[UIImage imageNamed:@"redButton"] stretchableImageWithLeftCapWidth: 14 topCapHeight: 22]  forState: UIControlStateNormal];
    
    DataRepository *globalData = [DataRepository sharedInstance];
    self.streetAddress.text        = globalData.unsentReport.streetAddress;
    self.additionalDirections.text = globalData.unsentReport.streetAddressDetails;
    
//    globalData.unsentReport.streetAddress        = self.streetAddress.text;
//    globalData.unsentReport.streetAddressDetails = self.additionalDirections.text;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_streetAddress release];
    [_intersection1 release];
    [_intersection2 release];
    [_additionalDirections release];
    [_saveButton release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [self setStreetAddress:nil];
    [self setIntersection1:nil];
    [self setIntersection2:nil];
    [self setAdditionalDirections:nil];
    [self setSaveButton:nil];
    [super viewDidUnload];
}



-(IBAction)setAddressTapped:(id)sender
{
    DataRepository *globalData = [DataRepository sharedInstance];
    globalData.locationAddress        = self.streetAddress.text;
    globalData.locationAddressDetails = self.additionalDirections.text;
    
    globalData.unsentReport.streetAddress        = self.streetAddress.text;
    globalData.unsentReport.streetAddressDetails = self.additionalDirections.text;
    
    [self dismissModalViewControllerAnimated: YES];
}



-(IBAction)cancelTapped:(id)sender
{
    [self dismissModalViewControllerAnimated: YES];
}



@end
